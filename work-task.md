#### TITLE
<hr/>
title_including_verb_noun_network_as_applicable
<hr/>

#### DESCRIPTION
<hr/>

description_of_what_and_why

**Definition of Done**

- list_of_end_state_items_once_task_completed

**Timeline**

time_period timebox_or_deadline
<hr/>


#### USAGE
<hr/>

Work tasks are the main task by which change is implemented, whether that be a task for IA, writing code for the deployment of infrastructure, or documenting a maintenance plan.  If the work is well understood, and doesn't provide direct user value then this is the task to use. Additionally the definition of done should not just include the direct task to be completed but any third order effects such as updating the rack elevations for newly racked equipment.