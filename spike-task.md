#### TITLE
<hr/>
SPIKE: research_task
<hr/>

#### DESCRIPTION
<hr/>

description_of_what_and_why

**Deliverables**

- given_document
- given_documents
- or_jira_issues

**Timeline**

time_period timebox_or_deadline
<hr/>


#### USAGE
<hr/>

Spikes describe a research task to be completed by a team member usually resulting in an Architectural Decision Record (ADR) or similar document.  They are used to explore ways ahead or to document assumptions before work starts down a given path.